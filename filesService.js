const fs = require('fs');
const path = require('path');

const my_password = 'nodejstop';

function createFile(req, res, next) {
  const ext = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
  const { filename, content, password } = req.body;
  
  if (!filename) {
    res.status(400).send({"message" : "Please enter filename"})
  } else if (!content || typeof content !== 'string') {
    res.status(400).send({ "message" : "Please specify 'content' parameter" });
  } else if (!ext.includes(filename.split('.').pop())) {
    res.status(400).send({ "message" : "Please use correct file extention"});
  } else if (password && password !== my_password) {
    res.status(403).send({ "message" : "Password is incorrect" });
  } else {
    fs.writeFile(`./files/${filename}`, content, (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
  }

  res.status(200).send({ "message": "File created successfully" });
}

function getFiles(req, res, next) {
  const listOfFiles = fs.readdirSync('./files/');
  fs.stat('./files', function(err) {
    if (!err) {
      res.status(200).send({
        "message": "Success",
        "files": listOfFiles
      });
    }
    else if (err.code === 'ENOENT') {
      res.status(400).send({ "message": "Client error" });
    }
  });
  
}

const getFile = (req, res, next) => {
  const { filename } = req.params;
  const { password } = req.body;
  const listOfFiles = fs.readdirSync('./files/');
  
  if (password && password !== my_password) {
    res.status(403).send({ "message" : "Password is incorrect" });
  } else if (listOfFiles.includes(`${filename}`)) {
    const content = fs.readFileSync(`./files/${filename}`, 'utf8');
    res.status(200).send({
          "message": "Success",
          "filename": filename,
          "content": content,
          "extension": filename.split('.').pop(),
          "uploadedDate": new Date()
        });
  } else {
    res.status(400).send({'message': 'file is not exist'});
  }
}

function editFile(req, res, next) {
  const { content } = req.body;
  const { filename } = req.params;

  fs.writeFile(`./files/${filename}`, content, (err) => {
    if (err) throw err;
    console.log('The file has been updated!');
  });

  res.status(200).send({
    "message": "Success updated",
    "filename": filename,
    "content": content
  });
}

function deleteFile (req, res, next) {
  const { filename } = req.params;

  fs.unlink(`./files/${filename}`, () => {
  });

  res.status(200).send({
    "message": "Success deleted"
  });

}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}